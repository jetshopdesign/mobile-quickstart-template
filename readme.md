# Mobile Quickstart Template

This is a package containing files to quick-start the design work for mobile sites.

This package will also streamline JS-development and provide a small function library.


## Install instructions

- Download the entire repo
- Unpack .zip file
- Zip the folder /app
- Restore in admin using the new .zip created from folder /app
- Done.


## Extra

**Add JS and CSS to Mobile Checkout:**

Admin > Settings > Script Management > MobileCheckout:

    <script src="/m/MarketDependentFrontFiles/M1/production/Scripts/main.js"></script>
    <link rel="stylesheet" type="text/css" href="/m/MarketDependentFrontFiles/M1/production/Css/mobile-checkout.css" />

Don't forget to check the box "Include script in head-tag"

sdfsdfsdf
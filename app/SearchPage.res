<j:Responsive_SearchPage_Starter />
<div class="productsearch-header">
  <h1 class="productsearch-name">
    <span class="search-nr-of-hits"><j:Responsive_SearchPage_TotalHits /></span><j:Responsive_SearchPage_NoOfHitsText />  <j:Responsive_SearchPage_ForQueryText />
    <span class="search-word"><j:Responsive_SearchPage_SearchQuery /></span>
  </h1>
</div>
<j:Responsive_SearchPage_SearchField />
<j:Responsive_SearchPage_Products />
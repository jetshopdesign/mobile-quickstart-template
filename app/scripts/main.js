﻿isStartPage = false;
isCheckoutPage = false;
isProductPage = false;
isCategoryPage = false;
isStandardPage = false;

$(document).ready(function () {


//TODO subCategory-dropDown < render as list, not select


    //
    // Detect page-type
    //
    if ($('script[src="/m/SharedFrontFiles/SystemScripts/startPage.js"]').length > 0) {
        initStartPage();
    }

    if (typeof JetshopData !== "undefined"){
        if (JetshopData.ProductId > 0) {
            initProductPage();
        }
        if (JetshopData.CategoryId > 0) {
            initCategoryPage();
        }
        if (JetshopData.PageId > 0) {
            initStandardPage();
        }
    }

    if ($("#mobilecheckout_upMobileCart").length > 0) {
        initCheckoutPage();
    }

    if (!isCheckoutPage) {
        // Hack to disable iOS zoom on input focus
        $('input:text,select,textarea').cancelZoom();
        // Set height of inner-wrap to max element's height
        var bodyScrollHeight = $(document).height(),
            navScrollHeight = $('#nav')[0].scrollHeight,
            maxHeight = Math.max(bodyScrollHeight, navScrollHeight);
        $('#inner-wrap').css('min-height', maxHeight + 'px');
        // Align product boxes vertically on cat page
        if (JetshopData.CategoryId && !JetshopData.ProductId) {
            autoSizeCatRows();
        }
    }
    // Get Initial Screen Dimensions
    var screenWidth = window.innerWidth,
        screenHeight = window.innerHeight;
});


//
// SET WINDOW RESIZE EVENTS
//
var windowResizeEvent;
$(window).resize(function () {
    clearTimeout(windowResizeEvent);
    windowResizeEvent = setTimeout(function () {
        // Align product boxes vertically on cat page
        if (JetshopData.CategoryId && !JetshopData.ProductId) {
            autoSizeCatRows();
        }
        // SET REDRAW OF LEFT NAV ON iOS8 DUE TO RENDERING BUG IN WEBKIT
        if (navigator.userAgent.match(/iPhone OS 8/)) {
            $("#nav").hide();
            setTimeout(function () {
                $("#nav").show();
            }, 1);
        }
    }, 100);
});


//
// Add lang attribute to html tag
//
if (typeof JetshopData != "undefined") {
    $('html').attr('lang', JetshopData.Language);
}

//
//  Init Start Page
//
function initStartPage() {
    $("body").addClass("start-page");
    if ($("#mainCategories").length == 1) {
        renderCategoryMenu();
        $(".category-list").remove();
    }
}

//
//  Init Product Page
//
function initProductPage() {
    isProductPage = true;
    $("body").addClass("product-page");
}

//
//  Init Category Page
//
function initCategoryPage() {
    isCategoryPage = true;
    $("body").addClass("category-page");
    $(".category-wrapper").addClass("clearfix");
    getCategoryDescription();
}

//
//  Init Standard Page
//
function initStandardPage() {
    isStandardPage = true;
    $("body").addClass("standard-page");
}

//
//  Init Checkout Page
//
function initCheckoutPage() {
    isCheckoutPage = true;
    $("body").addClass("checkout-page");
}

//
//  Init All Pages
//
function initAllPages() {

}


function renderCategoryMenu() {
    var temp = [];
    var string = "<ul class='subCategory-list'>";
    var categoryList = $(".JumpToMainCategoy option");
    categoryList.each(function () {
        var item = [];
        item.url = $(this).val();
        item.label = $(this).text();
        temp.push(item);
    });
    temp.splice(0, 1); // Remove the header
    for (var i = 0; i < temp.length; i++) {
        string += "<li><a class='list-item' href='" + temp[i].url + "'>" + temp[i].label + "</a></li>";
    }
    string += "</ul>";
    $("#category-list-placeholder").append(string);
}

// AUTO SIZE ALL PRODUCT ROW CONTENT VERTICALLY
function autoSizeCatRows(prodQtyInRow) {
    if (!prodQtyInRow) {
        if (window.innerWidth < 640) {
            prodQtyInRow = 2;
        }
        else {
            prodQtyInRow = 3;
        }
    }
    var prods = $(".product-wrapper");
    var prodQty = prods.length;
    var rowQty = Math.ceil(prodQty / prodQtyInRow);
    $(".product-names").css("height", "auto");
    for (i = 0; i < rowQty; i++) {
        var rowProds = prods.not(":gt(" + ((i * prodQtyInRow) + prodQtyInRow - 1) + ")").not(":lt(" + (i * prodQtyInRow) + ")");
        var highestNameHeight = 0;
        rowProds.each(function () {
            var name = $(this).find(".product-names");
            var nameHeight = name.height();
            if (nameHeight > highestNameHeight) {
                highestNameHeight = nameHeight;
            }
        });
        rowProds.find(".product-names").height(highestNameHeight);
    }
}

function renderCategoryInMainMenu(){
    //TODO there can't be any translations in this method
    var data = [];
    var menuItems = $('.JumpToMainCategoy option');
    var str = '<div class="nav-group cat-list"><ul><li style="font-size: 16px; margin-left: -7px;">Kategorier</li>';

    menuItems.each(function(){
        var temp = [];
        temp.name = $(this).text();
        temp.link = $(this).val();
        data.push(temp);
    });
    data.splice(0,1); // Remove the first item
    for (var i=0; data.length > i; i++) {
        console.log(data[i]);
        str += '<li><a class="new-category" href="'+ data[i].link +'">';
        str += data[i].name;
        str += '</a></li>';
    }
    str += '</ul></div>';
    $('#nav-search').first().after(str);
    $(".JumpToMainCategoy").parent().parent().remove()
}

function getCategoryDescription() {
    var categoryId = JetshopData.CategoryId;
    var prot = window.location.protocol;
    var restURL = prot + "//"+ document.domain +"/services/rest/v2/json/categories/" + categoryId;

    if (isInteger(categoryId)) {
        $.ajax({
            url: restURL, success: function (result) {
                var str = "<div class='rest-desc'>";
                if (typeof result[0] !== "undefined") {
                    if (typeof result[0].Header !== "undefined") {
                        str += "<h4>" + result[0].Header + "</h4>";
                    }
                    if (typeof result[0].Content !== "undefined") {
                        str += "<div class='inner'>" + htmlDecode(result[0].Content) + "</div>";
                    }
                    if (typeof result[0].Images[0] !== "undefined") {
                        str += "<img src='" + result[0].Images[0].Url + "' />";
                    }
                }
                str += "</div>";
                $("h1.category-name").after(str);
            }
        });
    }
}

function isInteger(n) {
    return n === +n && n === (n | 0);
}

function htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function htmlEntities(str) {
    return String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
}
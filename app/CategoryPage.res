<j:Responsive_CategoryPageStarter />
<div class="category-header">
	<h1 class="category-name"><j:Responsive_CategoryPage_CategoryName /></h1>
	<j:Responsive_CategoryPage_IfSubCategoriesAndProducts_Starter />
	    <div class="sub-category">
	        <div class="select-wrap">
				<j:Responsive_CategoryPage_SubCategoriesAsDropdown />
	        </div>
	    </div>
	<j:Responsive_CategoryPage_IfSubCategoriesAndProducts_Ender />
	<j:Responsive_CategoryPage_Sorting />
</div>
<j:Responsive_CategoryPage_IfSubCategoriesButNoProducts_Starter />
	<j:Responsive_CategoryPage_SubCategoriesAsList />
<j:Responsive_CategoryPage_IfSubCategoriesButNoProducts_Ender />
<j:Responsive_CategoryPage_Products />
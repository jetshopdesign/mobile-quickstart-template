<j:Responsive_CategoryPage_ProductListing_Starter />
<article class="product-wrapper">
    <a href="<j:Responsive_CategoryPage_ProductListing_Url />">
        <!-- Product Banner/Slider -->
        <figure class="product-banner">
            <img src="<j:Responsive_CategoryPage_ProductListing_Image />" alt="<j:Responsive_CategoryPage_ProductListing_Name />" />
        </figure>
        <!-- /Product Banner/Slider -->

        <!-- Headings -->
        <div class="product-names">
            <hgroup>
                <h2 class="product-name"><j:Responsive_CategoryPage_ProductListing_Name /></h2>
                <h3 class="product-sub-name"><j:Responsive_CategoryPage_ProductListing_SubName /></h3>
            </hgroup>
        </div>
        <!-- /Headings -->
        <j:Responsive_CategoryPage_ProductListing_ArticleNo />
        <j:Responsive_CategoryPage_ProductListing_StockStatus />
        <j:Responsive_CategoryPage_ProductListing_Prices />
        <j:Responsive_CategoryPage_ProductListing_Button />
    </a>		
</article>


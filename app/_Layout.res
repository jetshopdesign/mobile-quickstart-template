<j:Responsive_BasePageStarter />
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><j:Responsive_PageTitle /></title>
        <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
        <j:Responsive_SystemScripts />
        <j:Responsive_Javascript src="Scripts/vendor/cancel.zoom.js"/>
        <j:Responsive_Javascript src="Scripts/vendor/pageslide.nav.js"/>
        <j:Responsive_Javascript src="Scripts/main.js"/>
        <j:Responsive_Stylesheet href="Css/main.css"/>
    </head>
	<body>
		<div id="outer-wrap">
			<div id="inner-wrap">
				<!-- Global Header -->
				<header id="top" class="global-header">
					<div class="container">
						<ul class="global-header-toolbar-list">
							<!-- Main Navigation -->
							<li class="global-header-navigation">
								<a href="#nav" id="nav-open-btn" class="global-header-navigation-link">
									<i class="fa fa-bars"></i>
									<span class="visually-hidden">Main Navigation</span>
								</a>
							</li>
							<!-- /Main Navigation -->

						    <!-- Global Logo -->
							<li class="global-header-logo">
								<a class="global-header-logo-link" href="<j:Responsive_RootUrl />">
									<img class="global-header-logo-img" src="<j:Responsive_Logo />" alt="Logotype" />
									<!--<i class="fa fa-css3 global-header-logo-img"></i>-->
									<span class="visually-hidden">Logotype</span>
								</a>
							</li>
							<!-- /Global Logo -->
							<!-- Global Cart -->
						    <li class="global-header-cart">
        						<j:Responsive_Cart />
						    </li>
							<!-- /Global Cart -->

						</ul>
					</div>

				</header>
				<!-- /Global Header -->
			    <div id="middle" class="container">
				    <!-- Main Navigation -->
			        <nav id="nav" class="primary-navigation" role="navigation">
                <div class="cornerstone">
                  <!-- Return to content -->
                  <a class="close-btn" id="nav-close-btn" href="#top">
                    <i class="fa fa-times-circle"></i>
                    <j:Responsive_Texts_ReturnToContent />
                  </a>
                  <j:Responsive_Search />
                  <j:Responsive_ListOfPages />
                  <j:Responsive_MainCategoriesDropDown />
                  <j:Responsive_CurrenciesDropDown />
                  <j:Responsive_LanguagesDropDown />
                  <div class="external-nav-wrap">
                    <j:Responsive_GoToDesktopVersion />
                    <j:Responsive_PoweredByJetshop />
                  </div>
                </div>
			        </nav>
			        <main role="main" class="main">
                    	<j:Responsive_PageContent />
			        </main>
			    </div>
			</div>
		</div>
	</body>
</html>